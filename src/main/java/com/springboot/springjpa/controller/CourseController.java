package com.springboot.springjpa.controller;

import com.springboot.springjpa.model.Course;
import com.springboot.springjpa.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/*

 @Author melone
 @Date 7/16/18 
 
 */
@RestController
@RequestMapping("/courses")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @RequestMapping("")
    public List<Course> getCourses() {
        return courseService.getCourses();
    }

    @RequestMapping("/{id}")
    public Optional<Course> getCourseById(@PathVariable int id) {
        return courseService.getCourseById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void saveCourse(@RequestBody Course course) {
        courseService.addCourse(course);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyCourse(@PathVariable int id, @RequestBody Course course) {
        courseService.updateCourse(id, course);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeCourse(@PathVariable int id) {
        courseService.deleteCourse(id);
    }
}
