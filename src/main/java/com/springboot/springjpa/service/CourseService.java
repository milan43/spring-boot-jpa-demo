package com.springboot.springjpa.service;

import com.springboot.springjpa.model.Course;

import java.util.List;
import java.util.Optional;

/*

 @Author melone
 @Date 7/16/18 
 
 */
public interface CourseService {

    void addCourse(Course course);

    List<Course> getCourses();

    Optional<Course> getCourseById(int id);

    void deleteCourse(int id);

    void updateCourse(int id, Course course);
}
