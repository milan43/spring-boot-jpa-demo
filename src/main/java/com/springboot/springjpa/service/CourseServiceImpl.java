package com.springboot.springjpa.service;

import com.springboot.springjpa.model.Course;
import com.springboot.springjpa.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/*

 @Author melone
 @Date 7/16/18 
 
 */
@Repository
public class CourseServiceImpl implements CourseService{
    @Autowired
    private CourseRepository courseRepository;

    public void addCourse(Course course) {
        courseRepository.save(course);
    }

    public List<Course> getCourses() {
        return (List<Course>) courseRepository.findAll();
    }

    public Optional<Course> getCourseById(int id) {
        return courseRepository.findById(id);
    }

    public void deleteCourse(int id) {
        courseRepository.deleteById(id);
    }

    public void updateCourse(int id, Course course) {
        courseRepository.save(course);
    }
}
