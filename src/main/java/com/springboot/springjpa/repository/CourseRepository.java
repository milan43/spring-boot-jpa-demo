package com.springboot.springjpa.repository;

import com.springboot.springjpa.model.Course;
import org.springframework.data.repository.CrudRepository;

/*

 @Author melone
 @Date 7/16/18 
 
 */
public interface CourseRepository extends CrudRepository<Course, Integer> {

}
